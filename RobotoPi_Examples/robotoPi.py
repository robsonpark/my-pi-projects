__author__ = "Robson Soares"
__copyright__ = "Copyright 2018"

__license__ = ""
__version__ = "1.0"
__maintainer__ = "Robson Soares"
__email__ = "robson.soares@dce.ufpb.br"
__status__ = "Development"

'''
Requeriments:

sudo apt-get install python-dev python-rpi.gpio

'''

import RPi.GPIO as GPIO


class RobotoPi():
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(True)
    def exit(self):
        GPIO.cleanup()
        exit()
        
class PushButton():
    def __init__(self, pinNumber):
        self.pinNumber = pinNumber
        self.instanciatePin()
        
    def instanciatePin(self):
        GPIO.setup(self.pinNumber, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        
    #This method return True to pressed and False to unpressed
    def getState(self):
        if(GPIO.input(self.pinNumber)==1):
            return False
        else:
            return True
    #This method return the pin number associated
    def getPinNumber(self):
        return self.pinNumber
    
class Led():
    def __init__(self, pinNumber):
        self.pinNumber = pinNumber
        self.state = False
        GPIO.setup(pinNumber,GPIO.OUT)
        
    def getPinNumber(self):
        return self.pinNumber
    
    def setOn(self):
        GPIO.output(self.pinNumber,GPIO.HIGH)
        self.state = True
        
    def setOff(self):
        GPIO.output(self.pinNumber,GPIO.LOW)
        self.state = False
        
    def getState(self):
        return self.state


